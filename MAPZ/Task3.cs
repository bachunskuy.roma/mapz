﻿using System;
namespace MAPZ;

class ExampleClass
{
	int value;
	int value2 { get; set; }

	void ClassMethod()
	{
		value2 = 2;
		value = 1;
	}

	class ClassInside
	{
		int value3;
	}
}



class Child : ExampleClass
{
	//value = 100;
	//value2 = 11;
	//ClassMethod();
	//ClassInside.value3 = 9;
}

interface IExample
{
    void InterfaceMethod();
}

class classForI : IExample
{
	public void InterfaceMethod()
	{
		Console.WriteLine("Interface");
	}
}

struct ExampleStruct
{
    int value;
    void StructMethod()
    {
        value = 10;
    }
}


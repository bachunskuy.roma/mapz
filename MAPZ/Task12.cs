﻿using System;
using System.Diagnostics;
namespace MAPZ;

public class FirstClass
{
    public virtual void DoSomething()
    {
        Console.WriteLine("FirstClass is doing something.");
    }
}

public class SecondClass : FirstClass
{
    public override void DoSomething()
    {
        Console.WriteLine("SecondClass is doing something.");
    }
}

public class ThirdClass : SecondClass
{
    public override void DoSomething()
    {
        Console.WriteLine("ThirdClass is doing something.");
    }
}

public interface IDoSomething
{
    void DoSomething();
}

public class FirstInterfaceImplementation : IDoSomething
{
    public void DoSomething()
    {
        Console.WriteLine("FirstInterfaceImplementation is doing something.");
    }
}

public class SecondInterfaceImplementation : IDoSomething
{
    public void DoSomething()
    {
        Console.WriteLine("SecondInterfaceImplementation is doing something.");
    }
}

class Individual
{
    private const int numberOfIterations = 10000;
    /*static void Main()
    {
        Stopwatch stopwatchVirtual = new Stopwatch();
        Stopwatch stopwatchInterface = new Stopwatch();
        Stopwatch stopwatchReflection = new Stopwatch();
        stopwatchVirtual = TestVirtualMethod();
        stopwatchInterface = TestInterface();
        stopwatchReflection = TestReflection();

        Console.WriteLine($"Час виконання вiртуального методу: {stopwatchVirtual.ElapsedMilliseconds} мс");
        Console.WriteLine($"Час виконання iнтерфейсного методу: {stopwatchInterface.ElapsedMilliseconds} мс");
        Console.WriteLine($"Час виконання рефлексiйного методу: {stopwatchReflection.ElapsedMilliseconds} мс");

    }
    */
    static Stopwatch TestVirtualMethod()
    {
        FirstClass test1 = new ThirdClass();

        var stopwatch = Stopwatch.StartNew();
        for (int i = 0; i < numberOfIterations; i++)
        {
            test1.DoSomething();
        }
        stopwatch.Stop();
        return stopwatch;

    }

    static Stopwatch TestInterface()
    {
        IDoSomething test2 = new SecondInterfaceImplementation();

        var stopwatch = Stopwatch.StartNew();
        for (int i = 0; i < numberOfIterations; i++)
        {
            test2.DoSomething();
        }
        stopwatch.Stop();
        return stopwatch;
    }

    static Stopwatch TestReflection()
    {
        FirstClass test3 = new ThirdClass();
        var methodInfo = typeof(ThirdClass).GetMethod("DoSomething"); // отримання типу
        // метод GetMethod() приймає ім'я методу (DoSomething) і повертає об'єкт MethodInfo, який представляє інформацію про цей метод.

        var stopwatch = Stopwatch.StartNew(); ;
        for (int i = 0; i < numberOfIterations; i++)
        {
            methodInfo.Invoke(test3, null);
            //Invoke() викликає метод DoSomething для об'єкта, використовуючи отриманий об'єкт MethodInfo.
        }
        stopwatch.Stop();
        return stopwatch;
    }
}
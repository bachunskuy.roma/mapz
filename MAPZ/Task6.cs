﻿using System;
namespace MAPZ
{
	interface IFirst
	{
		void FirstMethod();
	}

	interface ISecond
	{
		void SecondMethod();
	}

	class First : IFirst
	{
		public void FirstMethod()
		{
			Console.WriteLine("FirstMethod Implementation");
		}
	}

	class Second : ISecond
	{
		public void SecondMethod()
		{
			Console.WriteLine("SecondMethod Implementation");

		}
	}

	class Third: IFirst, ISecond
	{
		void IFirst.FirstMethod()
		{
			Console.WriteLine("FirstMethod Implementation in third class");
		}

		void ISecond.SecondMethod()
		{
            Console.WriteLine("SecondMethod Implementation in third class");
        }
	}
}


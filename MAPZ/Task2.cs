﻿using System;
namespace MAPZ
{
	public class ParentClass
	{
		public int numPublic = 1;
		private int numPrivate = 2;
		protected int numProtected = 3;
		internal int numInternal = 4;
		private protected int numPrivateP = 5;
		protected internal int numProtectedI = 6;

        public void Show()
        {
            Console.WriteLine("Public: " + numPublic);
            Console.WriteLine("Private: " + numPrivate);
            Console.WriteLine("Protected: " + numProtected);
            Console.WriteLine("Internal: " + numInternal);
            Console.WriteLine("Private protected: " + numPrivateP);
            Console.WriteLine("Protected internal: " + numProtectedI);
        }
    }

	public class ChildClass : ParentClass
    {
        public void ShowParent()
        {
            Console.WriteLine("Public: " + numPublic);
           // Console.WriteLine("Private: " + numPrivate);
            Console.WriteLine("Protected: " + numProtected);
            Console.WriteLine("Internal: " + numInternal);
            Console.WriteLine("Private protected: " + numPrivateP);
            Console.WriteLine("Protected internal: " + numProtectedI);
        }
    }
}


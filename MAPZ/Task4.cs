﻿using System;
namespace MAPZ
{
	public class ClassOut
	{
		private class PrivateInnerClass
		{
			public void func() { Console.WriteLine("Hello"); }
		}

		PrivateInnerClass privateInnerClass = new PrivateInnerClass();

        protected class ProtectedInnerClass
        {
            public void func() { Console.WriteLine("Hello"); }
        }
        ProtectedInnerClass protectedInnerClass = new ProtectedInnerClass();

        internal class InternalInnerClass
        {
            public void func() { Console.WriteLine("Hello"); }
        }
        InternalInnerClass internalInnerClass = new InternalInnerClass();

        private protected class PrivateProtectedInnerClass
        {
            public void func() { Console.WriteLine("Hello"); }
        }
        PrivateProtectedInnerClass privateProtectedInnerClass = new PrivateProtectedInnerClass();

        protected internal class ProtectedInternalInnerClass
        {
            public void func() { Console.WriteLine("Hello"); }
        }
        ProtectedInternalInnerClass protectedInternalInnerClass = new ProtectedInternalInnerClass();


        public void call()
        {
            privateInnerClass.func();
            protectedInnerClass.func();
            internalInnerClass.func();
            privateProtectedInnerClass.func();
            protectedInternalInnerClass.func();
        }
    }

    /*class ClassoutTest
    {
        static void Main()
        {
            ClassOut test = new ClassOut();
            test.call();
        }
    }
    */
}


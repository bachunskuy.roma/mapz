﻿using System;
namespace MAPZ
{
	public interface IAnimal
	{
		void Speek();
		void Walk();
	}

	public abstract class Animal: IAnimal
	{
		protected string Name { get; set; }
        protected string Kind { get; set; }

		public abstract void Speek();
		public abstract void Walk();
    }

	public class Dog : Animal
	{
        public override void Speek()
        {
			Console.WriteLine("Woof Woof");
        }
        public override void Walk()
        {
            Console.WriteLine("Walking Dog!");
        }
    }
}
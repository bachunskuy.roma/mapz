﻿using System;
namespace MAPZ
{
	class StaticDynamicClass
	{
		public static int staticField;

		public int dynamicField;

		static StaticDynamicClass()
		{
			staticField = 0;
			Console.WriteLine($" Static constructor initialized static field: {staticField}");
		}

		public StaticDynamicClass(int i)
		{
			dynamicField = i;
            Console.WriteLine($" Dynamic constructor initialized dynamic field: {dynamicField}");
        }

		public StaticDynamicClass()
		{
			staticField = 1;
			dynamicField = 1;
			Console.WriteLine($" Base constructor initialized dynamic field: {dynamicField} and static field: {staticField}");
		}

		public void Display()
		{
			Console.WriteLine($"Static field: {staticField}");
            Console.WriteLine($"Dynamic field: {dynamicField}");
        }
    }

	/*class StaticDynamicClassTest
	{
		static void Main()
		{
			StaticDynamicClass test1 = new StaticDynamicClass(15);
			test1.Display();
			Console.WriteLine("\n");

			StaticDynamicClass test2 = new StaticDynamicClass();
			test2.Display();
            Console.WriteLine("\n");

            StaticDynamicClass test3 = new StaticDynamicClass()
			{
				dynamicField = 23

			};
			test3.Display();
            Console.WriteLine("\n");
        }

    }*/
}


﻿/*
namespace MAPZ;

using System;

class OutRefTest
{
    static void Main()
    {
        int a = 10;
        int b = 20;
        int result;

        SumWithoutOutRef(a, b);
        Console.WriteLine($"a = {a}, b = {b}");

        SumWithOut(a, b, out result);
        Console.WriteLine($"a = {a}, b = {b}, result = {result}");

        SumWithRef(ref a, ref b);
        Console.WriteLine($"a = {a}, b = {b}");

        RefClass obj = new RefClass(10);
        Console.WriteLine("Before: " + obj.Value);

        ChangeObject(obj);
        Console.WriteLine("After (none): " + obj.Value);

        ChangeObjectWithOut(out obj);
        Console.WriteLine("After (out): " + obj.Value);

        ChangeObjectWithRef(ref obj);
        Console.WriteLine("After (ref): " + obj.Value);
    }

    static void SumWithoutOutRef(int x, int y)
    {
        x += y;
    }

    static void SumWithOut(int x, int y, out int result)
    {
        result = x + y;
    }

    static void SumWithRef(ref int x, ref int y)
    {
        x += 20;
        y += 20;
    }

    static void ChangeObject(RefClass obj)
    {
        obj = new RefClass(20);
    }
    static void ChangeObjectWithOut(out RefClass obj)
    {
        obj = new RefClass(20);
    }

    static void ChangeObjectWithRef(ref RefClass obj)
    {
        obj = new RefClass(30);
    }
}

class RefClass
{
    public int Value;

    public RefClass(int value)
    {
        Value = value;
    }
}
*/
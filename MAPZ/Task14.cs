﻿namespace MAPZ;

public class Person
{
    public string Name { get; set; }
    public int Age { get; set; }

    public Person(string name, int age)
    {
        Name = name;
        Age = age;
    }

    public override string ToString()
    {
        return $"{Name} {Age}";
    }

    public override bool Equals(object obj)
    {
        if (obj == null || !(obj is Person))
            return false;

        Person other = (Person)obj;
        return this.Name == other.Name && this.Age == other.Age;
    }

    public override int GetHashCode()
    {
        return Name.GetHashCode() ^ Age.GetHashCode();
    }
}
/*
class ObjectOverriding
{
    static void Main(string[] args)
    {
        Person person1 = new Person("John", 30);
        Person person2 = new Person("Ivan", 45);

        Console.WriteLine("person1: " + person1);
        Console.WriteLine($"person2: {person2}");

        Console.WriteLine("Чи рiвнi об'єкти person1 та person2? " + person1.Equals(person2));

        Console.WriteLine("Хеш-код person1: " + person1.GetHashCode());
        Console.WriteLine("Хеш-код person2: " + person2.GetHashCode());

    }
    
}
*/
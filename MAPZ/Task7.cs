﻿using System;
namespace MAPZ
{
	class BaseClass
	{

		protected int BaseNumber;
		public BaseClass()
		{
			Console.WriteLine("Base number:");
		}

		public BaseClass(int BaseNum)
		{
			this.BaseNumber = BaseNum;
			Console.WriteLine($"New base number is : {BaseNumber}");
		}

		public void Display()
		{
            Console.WriteLine($"Base number  is : {BaseNumber}");
        }
		
	}

	class DerivedClass : BaseClass
	{
		private int DerivedNumber;

		public DerivedClass(int DerivedNum, int BaseNumber) : base(BaseNumber)
		{
			this.DerivedNumber = DerivedNum;
			Console.WriteLine($"Child number is : {DerivedNumber} and base number is : {BaseNumber}");
		}

		public void DisplayDerived()
		{
			Console.WriteLine($"Derived number is {DerivedNumber}");
		}

		public void DisplayAll()
		{
			base.Display();
			Console.WriteLine($"Derived number is {DerivedNumber}");
		}
	}
}


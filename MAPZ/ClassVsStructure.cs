﻿using System;
using System.Diagnostics;

public class MyClass
{
    public int MyField1;
    public int MyField2;
    public int MyField3;
}

public struct MyStruct
{
    public int MyField1;
    public int MyField2;
    public int MyField3;
}

class Program
{
    static void Main(string[] args)
    {
        const int count = 10_000_000; // Кількість об'єктів для створення

        // Вимірюємо час створення об'єктів класу MyClass
        Stopwatch classStopwatch = Stopwatch.StartNew();
        MyClass[] classArray = new MyClass[count];
        for (int i = 0; i < count; i++)
        {
            classArray[i] = new MyClass();
        }
        classStopwatch.Stop();
        Console.WriteLine($"Час створення {count} об'єктів класу MyClass: {classStopwatch.ElapsedMilliseconds} мс");

        // Вимірюємо час створення об'єктів структури MyStruct
        Stopwatch structStopwatch = Stopwatch.StartNew();
        MyStruct[] structArray = new MyStruct[count];
        for (int i = 0; i < count; i++)
        {
            structArray[i] = new MyStruct();
        }
        structStopwatch.Stop();
        Console.WriteLine($"Час створення {count} об'єктів структури MyStruct: {structStopwatch.ElapsedMilliseconds} мс");

        // Вимірюємо зміни оперативної пам'яті
        long classMemory = GC.GetTotalMemory(true);
        Console.WriteLine($"Оперативна пам'ять після створення об'єктів класу MyClass: {classMemory} байт");
        long structMemory = GC.GetTotalMemory(true);
        Console.WriteLine($"Оперативна пам'ять після створення об'єктів структури MyStruct: {structMemory} байт");
    }
}